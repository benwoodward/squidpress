desc "Generate jekyll site"
task :generate do
  puts "## Generating Site with Jekyll"
  system "compass compile --css-dir source/stylesheets"
  system "jekyll"
end

desc "Default deploy"
task :deploy do
  Rake::Task[:generate].execute
  Rake::Task[:push].execute
end

desc "deploy public directory to github pages"
multitask :push do
  puts "## Deploying branch to Github Pages "
  (Dir["deploy_dir/*"]).each { |f| rm_rf(f) }
  puts "\n## copying public} to deploy_dir"
  cp_r "public/.", "deploy_dir"
  cd "deploy_dir" do
    system "git add ."
    system "git add -u"
    puts "\n## Commiting: Site updated at #{Time.now.utc}"
    message = "Site updated at #{Time.now.utc}"
    system "git commit -m \"#{message}\""
    puts "\n## Pushing generated deploy_dir website"
    system "git push origin master --force"
    puts "\n## Github Pages deploy complete"
  end
end

desc "Set up deploy folder (deploy_dir) and nested repo Github Pages deployment"
task :setup_github_pages do
  rm_rf 'deploy_dir'
  mkdir 'deploy_dir'

  # declare variable in outer scope for access
  user = ''

  cd "deploy_dir" do
    system "git init"
    system "git add ."
    system "git commit -m \"Initial Commit\""

    # add remote to the new repo
    puts "Enter the read/write url for your repository" 
    puts "(For example, 'git@github.com:your_username/your_username.github.com)"
    repo_url = get_stdin("Repository url: ")
    user = repo_url.match(/:([^\/]+)/)[1]
    system "git remote add origin #{repo_url}"
    puts "Added remote #{repo_url} as origin to nested deploy repo (/deploy_dir/.git)"
  end
  url = "http://#{user}.github.com"
  puts "\n---\n## Now you can deploy to #{url} with `rake deploy` ##"
end

desc "Watch the site and regenerate when it changes"
task :watch do
  puts "Starting to watch source with Jekyll and Compass."
  system "compass compile --css-dir source/stylesheets"
  jekyllPid = Process.spawn("jekyll --server --auto")
  compassPid = Process.spawn("compass watch")

  trap("INT") {
    [jekyllPid, compassPid].each { |pid| Process.kill(9, pid) rescue Errno::ESRCH }
    exit 0
  }

  [jekyllPid, compassPid].each { |pid| Process.wait(pid) }
end

def get_stdin(message)
  print message
  STDIN.gets.chomp
end

