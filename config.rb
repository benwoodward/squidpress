# this is not a rails project
project_type = :stand_alone

# Publishing paths
http_path = "/"
http_images_path = "/images"
http_fonts_path = "/fonts"
css_dir = "source/site/stylesheets"

# Local development paths
sass_dir = "source/sass"
images_dir = "source/site/images"
fonts_dir = "source/site/fonts"

# don't add comments to compiled code
line_comments = false

output_style = :compressed
